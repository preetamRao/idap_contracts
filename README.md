# QUILLPLAY-SMART-CONTRACTS

Steps for smart contracts testing and deployment

## Prerequisites
  - Install `node.js`
  - `npm install -g truffle@4.1.5`


### 1. Truffle compile and migrate

  #### 1a. If outside  the development console we don't preface commands with truffle.

  - `Truffle develop`
  - `compile`
  - `migrate`


  #### 1b. If  outside the development console(run ganache or according to truffle configuration) we don't preface commands with truffle.

  - `truffle compile`
  - `truffle migrate` (If no new migrations exists, truffle migrate won't perform any action at all)
or
-   `truffle migrate --reset` (You can use the --reset option to run all your migrations from the beginning)

  -   Note to Mac OS X users: Truffle is sometimes confused by .DS_Store files. If you get an error mentioning one of those files, just delete it.


  #### 1c. truffle inject contract name as variable in truffle develop console after Migrations

  - ```javascript
        truffle(develop)> crowdSale
        { [Function: TruffleContract]
        _static_methods:
    ```

  #### 1d. Now to interact with smart contract we set instance of contract in a variable

     ```javascript
      truffle(develop) > var inst;

      truffle(develop) > crowdSale.deployed().then((instance) => inst = instance;)
     ```
  #### 1e. Now we can call function like that

    ```javascript
      truffle(develop) >  crowdSale.anyFunction().call(...arguments).then(console.log)
    ```

### 2. Truffle can run tests written in Solidity or JavaScript against your smart contracts. Note the command varies slightly if you're in or outside of the development console.

  ```
    // If inside the development console.
    test

    // If outside the development console..
    truffle test --network development
  ```

### 3. Token Vesting

  #### 3a. `Constructor : IdapToken(uint _initialSupply,address _tokensOwner)`

  ```
     - Params:
        1.Initial supply of tokens it includes team and backers rewards.

        2.address of token owner(optional).By default owner will set to the creater of the contract.
      - Working : This function will return the currently transferable tokens of the passed address.

  ```

  #### 3b. `Function name  : grantToBeneficiary(uint _value,address _beneficiaryAddress,bool _revokable,uint _startTime,uint _endTime,uint _cliff,string _beneficiaryRole)`
  ```
   - Params:
     1. Value to be grant to beneficiary
     2. Address of the beneficiary
     3. Owner can remove vesting policy if revokable is true
     4. Vesting start unix time ,
     5. Vesting endtime unix time stamp
     6. Cliff timestamp
     7. Beneficary role can be team investor or advisor
   - Working : This function can only be called by owner of contract to grant some tokens to team members,private investors and advisors.It will
    store the the beneficiary details in a mapping storage named as **benfeciaryGrants** in the contract

    ```
  #### 3c. `Function name :CalculateVestedTokens(address _beneficiaryAddress,uint _anyTimeStamp)`

```
  - Params:
    1. address of beneficiary.
    2.unix timestamp
  - Working : This is a constant function.It will return the tokens which has been vested till the passed timestamp.This function will calculate
  tokens linearly.Beneficiary cannot transfer tokens before cliff time.After cliff period tokens will be vested linearly ,However tokens granted
  to beneficiary will show up in the balance but he/she can only transfer vested tokens which has not been used yet.If cliff time is set equal to
  starttime of vesting then beneficiary can start sending tokens from the time of begining but still beneficiary cannot transfer all tokens.He/she
  can only tranfer vested tokens from start time to current time

    ```

  #### 3d. `Function name : currentTransferableTokens(address benefeciary)`

  ```
    - Params:
      1.address of the beneficiary
    - Working : This function will return the currently transferable tokens of the passed address.
    -
  ```

  #### 3f. `Function name : getCurrentVestedTokens()`
  ```
    - Params:
    - Working :This function will return the currently vested tokens of the sender .It will include the tokens which has been used .
    ```

  #### 3g.  `Function name : revokeVesting(address beneficiary)`
  ```
  - Params:
    1.address of the beneficiary
  - Working : This function will revoke vesting boundation from beneficiary  and now beneficiary can transfer his tokens which was grant to him
  by owner without any vesting bounding.This function can only be called by owner.This function is useful in situation when a beneficiary ex(team member) completes his work before vesting period ,so tokens owner can remove the vesting boundation from beneficiary and after that
  beneficiary can tranfer all the tokens granted to him initialy.
  ```
  #### 3h. `Function name : transfer(address _to, uint256 _value)`

  ```  
- Params:
    1. address of recipient ,
    2.number of tokens to be transferred
  - Working : This function overrides standardToken `transfer` function.This function will check if sender address if granted vesting
  value or not and based on that will allow currently transferable tokens to be transfered by sender.If sender is not present in ` beneficiaryGrants`
  mapping than sender can transfer his tokens normally.
  ```
  #### 3i. `Function name : stopVestingAndRefund(address _beneficiary)`

  ```
    Params:
      1.address of the beneficiary
    - Working : This function will deduct non vested tokens from beneficiary balance and refund only those vested tokens to beneficiary which are
      not used by beneficiary.This function is useful for token owner when any beneficiary will left in between the vesting period.So owner will only give already vested tokens only.
      ```
  #### 3j. Running tests:-
  ```
      truffle test --network development
      ==> For solidity coverage
      ./node_modules/.bin/solidity-coverage
    ==> If facing error in running solidity coverage ,try to run coverage on different port or kill process on existing port

  ```

### How to Deploy and Test Manually

-  Deploy IdapToken — set total supply and funds wallet (which will have total tokens)
-  Deploy CrowdSale -  (set constructor variables - with token address)
-  Transfer CrowdSale token  (with decimals ) from IdapToken to CrowdSale address ,
   this can only done by admin (so after this CrowdSale will have ico sale token)
-  Change the token admin from xyz to CrowdSale application address. (and it can be done by present admin) ,
   because  in CrowdSale period only admin can transfer token , and CrowdSale address is calling IdapToken ,
   so permission should be with CrowdSale
-  Verify that your CrowdSale application is the admin of token and must be able to mint/transfer (we have only transfer )
   tokens during ICO period

  `Example`

-  Total tokens = 155294118 (155 million)
-  155294118000000000000000000
-  ico sale tokens  =  66000000
-  if token in decimals , so during transfer we have to insert amount with decimals in mist/remix wallet
-  IdapToken address 0xAF60e94FA4E955cCA845FB3b597503c5506f81fE
-  CrowdSale address - 0xa6Fc7Ec373501F58c13f751532e07e512dc35c48
-  funds wallet - address = 0xbc3F6926F4bd5fE47C1A344B0218bBF1B374FA3B

-  funding start block - 256 bits unsigned integer = 1355860
-  first in hours - 256 bits unsigned integer 5
-  second in hours - 256 bits unsigned integer 10
-  third in hours - 256 bits unsigned integer 15
-  funding duration in hours - 256 bits unsigned integer 20
-  token address - address



### Minimum testing:

- Have a testing plan for both verification and validation testing
- Do code reviews
- Do verification testing with automated unit, functional and integration tests
- Do external security audits
- Do validation testing by extensively testing on the testnet and then
- After testnet testing, do more testing with limited risk in proof-of-concept or alpha stage on the main net


### To test in testnet(ropsten,rinkbey)

- Run geth in testnet mode, with RPC server enabled:
  geth --testnet --rpc console 2>> geth.log

  let’s create an account: (remember the password!)
  ```> personal.newAccount()
  Passphrase:
  Repeat passphrase:
  "0xa88614166227d83c93f4c50be37150b9500d51fc"

  ```

- To check you balance, run:
  ```> eth.getBalance(eth.accounts[0])
  0
  ```
- It will show no balance because your node hasn’t synced with the rest of the network yet. While you wait for   that, check your balance in a testnet block explorer

  - Once your node has synced, you’re ready to deploy the contracts to the testnet using Truffle. First, unlock your main geth account, so that Truffle can use it. And be sure that it holds some balance, or you won’t be able to push a new contract to the network. On geth run:
  ```> personal.unlockAccount(eth.accounts[0], "mypassword", 24*3600)
  true
  > eth.getBalance(eth.accounts[0])
  1000000000000000000
  Ready to go! If some of these two are not working for you, check the steps above and make sure you’ve completed them correctly. Now run:
  $ truffle migrate --reset
  ```

### FAQ

* _`Remember` Why is there both a truffle.js file and a truffle-config.js file?_

  `truffle-config.js` is a copy of `truffle.js` for compatibility with Windows development environments. Feel free to it if it's irrelevant to your platform.
