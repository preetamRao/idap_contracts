//..............................................
//.IIIII.DDDDDDDDD.......AAAAA.....PPPPPPPPP....
//.IIIII.DDDDDDDDDD......AAAAA.....PPPPPPPPPP...
//.IIIII.DDDDDDDDDDD.....AAAAAA....PPPPPPPPPPP..
//.IIIII.DDD....DDDDD...AAAAAAA....PPPP...PPPP..
//.IIIII.DDDD....DDDD...AAAAAAA....PPPP...PPPP..
//.IIIII.DDDD....DDDD..AAAAAAAAA...PPPPPPPPPPP..
//.IIIII.DDDD....DDDD..AAAA.AAAA...PPPPPPPPPPP..
//.IIIII.DDDD....DDDD..AAAAAAAAAA..PPPPPPPPPP...
//.IIIII.DDDD....DDDD.AAAAAAAAAAA..PPPPPPPPP....
//.IIIII.DDD....DDDD..AAAAAAAAAAA..PPPP.........
//.IIIII.DDDDDDDDDDD..AAAAAAAAAAAA.PPPP.........
//.IIIII.DDDDDDDDDD..AAAA.....AAAA.PPPP.........
//.IIIII.DDDDDDDDD...AAAA.....AAAA.PPPP.........
//...............https://www.idap.io............

pragma solidity 0.4.21;
import "./base/StandardToken.sol";
import "./base/Ownable.sol";

contract IdapToken is StandardToken, Ownable {

//Begin: state variables
    string public constant name = "IdapToken"; // solhint-disable-line
    string public constant symbol = "IDP"; // solhint-disable-line
    uint public constant decimals = 18; // solhint-disable-line
    bool public fundraising = true;
    //total vesting which is already done;
    uint public totalVestedAmount;

    Ownable public ownable;

    struct BenfeciaryGrant {
        address beneficiaryAddress;
        uint vestingParts;
        uint tokensValue;
        bool revokable;
        uint revokedBlock;
        uint startBlock;
        uint endBlock;
        uint cliffBlock;
        uint durationInHours;
        bool revoked;
        uint usedTokens;
        string beneficiaryRole; //team member,private investor,advisors
    }

    mapping(address => BenfeciaryGrant) benefeciaryGrants;
    mapping (address => bool) public frozenAccounts;
//End: state variables

//Begin: events
    event FrozenFund(address target, bool frozen);
//End: events

//Begin: modifiers


    modifier manageTransfer() {
        if (msg.sender == owner) {
            _;
        }
        else {
            require(fundraising == false);
            _;
        }
    }
//End: modifiers

//Begin: constructor
    function IdapToken(uint initialSupply, address _tokensOwner) public Ownable(_tokensOwner) {
        require(initialSupply > 0);
        require(_tokensOwner != 0x0);
        totalSupply = initialSupply * (uint256(10) ** decimals);
        balances[owner] = totalSupply;
        emit Transfer(0x0, owner, totalSupply); // solhint-disable-line
    }

    function freezeAccount (address target, bool freeze) public onlyOwner {
        require(target != 0x0);
        require(freeze == (true || false));
        frozenAccounts[target] = freeze;
        emit FrozenFund(target, freeze); // solhint-disable-line
    }

//End: constructor

    //granting tokens to beneficiaries which they can transfer only after a certain vesting period
    function grantToBeneficiary(
        uint _value,
        uint _vestingParts,
        address _beneficiaryAddress,
        bool _revokable,
        uint _startBlock,
        uint _cliffBlock,
        uint _durationInHours,
        uint _averageBlockTime,
        string _beneficiaryRole) public onlyOwner {
        require(_beneficiaryAddress != address(0));
        require(balances[owner] > _value);
        require(_revokable == (true || false));
        require(_startBlock > 0);
        require(_cliffBlock >= _startBlock);
        require(_durationInHours > 0);
        require(_averageBlockTime > 0);
        require(bytes(_beneficiaryRole).length > 0);
        require(benefeciaryGrants[_beneficiaryAddress].beneficiaryAddress != _beneficiaryAddress);
        uint blocksMinedInHour = 3600 / _averageBlockTime;
        uint _endBlock = _startBlock.add(_durationInHours.mul(blocksMinedInHour));
        benefeciaryGrants[_beneficiaryAddress].vestingParts = _vestingParts;

        benefeciaryGrants[_beneficiaryAddress].beneficiaryAddress = _beneficiaryAddress;
        benefeciaryGrants[_beneficiaryAddress].tokensValue = _value;
        benefeciaryGrants[_beneficiaryAddress].revokable = _revokable;
        benefeciaryGrants[_beneficiaryAddress].startBlock = _startBlock;
        benefeciaryGrants[_beneficiaryAddress].endBlock = _endBlock;
        benefeciaryGrants[_beneficiaryAddress].cliffBlock = _cliffBlock;
        benefeciaryGrants[_beneficiaryAddress].durationInHours = _durationInHours;
        benefeciaryGrants[_beneficiaryAddress].beneficiaryRole = _beneficiaryRole;
        totalVestedAmount = totalVestedAmount.add(_value);
        super.transfer(_beneficiaryAddress, _value);
    }

    //calculating vested tokens according to blockNumber
    function calculateVestedTokens(address _beneficiaryAddress, uint _blockNumber) public constant returns (uint) {
        require(_beneficiaryAddress != 0x0);
        require(_blockNumber > 0);
        BenfeciaryGrant storage grant = benefeciaryGrants[_beneficiaryAddress];
        if (grant.tokensValue == 0) {
            return 0;
        }

        if (_blockNumber < grant.cliffBlock) {
            return 0;
        }

        if (_blockNumber >= grant.endBlock) {
            return grant.tokensValue;
        }

        uint blockDifference =  grant.endBlock.sub(grant.startBlock);
        uint valueWithoutDecimal = grant.tokensValue / (uint256(10) ** decimals);
        uint tokensMulBlocks = valueWithoutDecimal.mul((_blockNumber.sub(grant.startBlock)));
        return tokensMulBlocks / blockDifference;
    }

    //visiblity is external only for testing purpose
    //@TODO: remove external , add private
    function newCurrentTransferableTokens(address _beneficiary) external constant returns (uint) {
        BenfeciaryGrant storage grant = benefeciaryGrants[_beneficiary];
        uint tokensWithoutDECIMALS = grant.tokensValue / (uint256(10) ** decimals);
        uint totalBlocks = grant.durationInHours.mul(240);
        uint blockInterval = totalBlocks / grant.vestingParts;

        uint currentDifference = block.number - grant.startBlock;
        if (currentDifference >= blockInterval && block.number < grant.endBlock && block.number > grant.cliffBlock) {
            uint counterfactor =  currentDifference / blockInterval;
            uint currentTokens = counterfactor.mul((tokensWithoutDECIMALS / grant.vestingParts));
            return currentTokens - grant.usedTokens;
        }
        else if (block.number >= grant.endBlock) {
            return tokensWithoutDECIMALS - grant.usedTokens;
        }
        return 0;
    }

    // tokens which can be transferred at current time according to vesting policy. (uncommnet and replace ,we have to have use linear vesting)
    // function currentTransferableTokens(address benefeciary) private constant returns (uint) {
    //     require(benefeciary != 0x0);
    //     uint transferableTokens;
    //     BenfeciaryGrant storage grant = benefeciaryGrants[benefeciary];
    //     if (grant.revoked == true) {
    //         transferableTokens = grant.tokensValue - grant.usedTokens;
    //         return transferableTokens;
    //     }
    //     else {
    //         transferableTokens = calculateVestedTokens(benefeciary, block.number).sub(grant.usedTokens);
    //         return transferableTokens;
    //     }
    // }

    //total tokens vested between starting of vesting block and current block number.It includes previously transferred tokens if any.
    function getCurrentVestedTokens(address _beneficiary) public constant returns (uint) {
        require(_beneficiary != 0x0);
        return calculateVestedTokens(_beneficiary, block.number);
    }

  //revoke the vesting from beneficiary and refund already vested tokens which are not yet transferred by beneficiary.

    function revokeVesting(address beneficiary) public onlyOwner {
        require(beneficiary != 0x0);
        BenfeciaryGrant storage grant = benefeciaryGrants[beneficiary];
        grant.revoked = true;
        grant.revokedBlock = block.number;
    }

// stop vesting in between start and end block and refund only already vested unused tokens.
    function stopVestingAndRefund(address beneficiary) public onlyOwner {
        require(beneficiary != 0x0);
        BenfeciaryGrant storage grant = benefeciaryGrants[beneficiary];
        require(grant.revoked != true);
        balances[beneficiary] = balances[beneficiary].sub(grant.tokensValue.sub(grant.usedTokens));
        uint refund = newCurrentTransferableTokens(beneficiary);
        balances[beneficiary] = balances[beneficiary].add(refund);
    }
   //show the token vesting details of beneficiary
    function getVestingTokensDetail(address _beneficiary) public view returns(uint, uint, uint) {
        BenfeciaryGrant storage grant = benefeciaryGrants[_beneficiary];
        uint _release = newCurrentTransferableTokens(_beneficiary); // total releaseAble tokens from start block number to current block number
        uint _usedTokens = grant.usedTokens;   // tokens used by user from vested tokens
        uint _grantTokens = grant.tokensValue;        // total tokens granted to user .

        return (_release, _usedTokens, _grantTokens);

    }
   //show the vesting schedule of beneficiary
    function getVestingSchedule(address _beneficiary) public view returns(uint, uint, uint, uint, bool) {
        require(_beneficiary != 0x0);
        BenfeciaryGrant storage grant = benefeciaryGrants[_beneficiary];
        uint start = grant.startBlock; //vesting start block
        uint end = grant.endBlock;   // vesting end block
        uint _duration = grant.durationInHours; // vesting duration in hours
        uint _vestingParts = grant.vestingParts;
        uint _revokedBlock = grant.revokedBlock;
        bool _revoked = grant.revoked;
        return(start, end, _duration, _vestingParts, _revokedBlock, _revoked);

    }

//Begin: overriden methods

    function transfer(address _to, uint256 _value) public manageTransfer onlyPayloadSize(2) returns (bool success) {
        require(_to != address(0));
        require(!frozenAccounts[msg.sender]);
        BenfeciaryGrant storage grant = benefeciaryGrants[msg.sender];

        if (grant.tokensValue > 0 && grant.revoked != true) {
            uint vestedTokens = calculateVestedTokens(msg.sender, block.number);

            if (vestedTokens == 0) {
                return false;
            }

            uint transferableTokens = vestedTokens.sub(grant.usedTokens);
            require(transferableTokens >= _value);
            if (transferableTokens == 0) {
                return false;
            }
            grant.usedTokens = grant.usedTokens.add(_value);
            totalVestedAmount = totalVestedAmount.sub(_value);
            return super.transfer(_to, _value); //if transfer is initiated by team,private investor or advisor
        }
        else {
            return super.transfer(_to, _value); // transfer initiated by normal buyer.
        }
    }

    function transferFrom(address _from, address _to, uint256 _value)
        public
        manageTransfer
        onlyPayloadSize(3) returns (bool success)
    {
        require(_to != address(0));
        require(_from != address(0));
        require(!frozenAccounts[msg.sender]);

        BenfeciaryGrant storage grant = benefeciaryGrants[_from];

        if (grant.tokensValue > 0 && grant.revoked != true) {
            uint vestedTokens = calculateVestedTokens(_from, block.number);
            if (vestedTokens == 0) {
                return false;
            }
            uint transferableTokens = vestedTokens.sub(grant.usedTokens);
            require(transferableTokens >= _value);
            if (transferableTokens == 0) {
                return false;
            }
            grant.usedTokens = grant.usedTokens.add(_value);
            totalVestedAmount = totalVestedAmount.sub(_value);
            return super.transferFrom(_from, _to, _value); //if transfer is initiated by team,private investor or advisor
        }
        else {
            return super.transferFrom(_from, _to, _value); // transfer initiated by normal buyer.
        }
    }

//End: overriden methods


//Being: setters

    function burn(uint256 _value) public onlyOwner returns (bool burnSuccess) {
        require(fundraising == false);
        return super.burn(_value);
    }

    function finalize() public  onlyOwner {
        require(fundraising != false);
        // Switch to Operational state. This is the only place this can happen.
        fundraising = false;
    }


//End: setters

    function() public {
        revert();
    }

}
