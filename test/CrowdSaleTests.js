
const idap = artifacts.require('./IdapToken.sol');
const sale = artifacts.require('./CrowdSale.sol');
const web3 = require('web3');
const Web3 = new web3(new web3.providers.HttpProvider('http://localhost:8545'));


//const Web3 = require('web3');
//const  web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
contract(sale,(accounts)=>{
  let token;
  let app;

  //let crowdApp;
  //let crowdSale;
  it('should get tokens transfered by token contract',()=>{
    token = idap.at('0xcfeb869f69431e42cdb54a4f4f105c19c080a601');
    sale.deployed()
      .then((instance)=>{
        app = instance;
        return app.owner.call();

      })
      .then((appOwner)=>{
        console.log(appOwner);
        return token.owner.call();
      })
      .then((tokenOwner)=>{
        console.log(tokenOwner);
        return token.transfer(app.address,5000000000000000000000000000000000000);
      })
      .then(()=>{

        return token.transferOwnership(app.address);
      })
      .then(()=>{
        return token.owner.call();

      })
      .then((newTokenOwner)=>{
        console.log(newTokenOwner);
        return token.balanceOf(app.address);

      })
      .then((saleBalance)=>{
        console.log(saleBalance.toNumber());
        console.log(app.address);
        return app.sendTransaction({ from:accounts[3],value : Web3.toWei(2, 'ether') });
      });

  });
});
